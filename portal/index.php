<?php
session_start();
include "../app/koneksi.php";
$id_user = $_SESSION['id_user'];
$level = $_SESSION['level'];
$nama_user = $_SESSION['nama_user'];
$q = mysqli_query($connect, "SELECT * from tbl_user where id_user ='$id_user'");
if (mysqli_num_rows($q) == 0) {
    header("location:../page/error.php");
} else {
?>

    <html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Portal Telkom Akses</title>
        <link rel="icon" type="image/png" sizes="16x16" href="../public/image/logo.png">

        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="../public/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
        <!-- Toastr -->
        <link rel="stylesheet" href="../public/plugins/toastr/toastr.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="../public/plugins/select2/css/select2.min.css">
        <link rel="stylesheet" href="../public/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../public/plugins/fontawesome-free/css/all.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../public/dist/css/adminlte.min.css">
        <!-- fullcalendar -->
        <link rel="stylesheet" href="../public/css/fullcalendar.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="../public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="../public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
        <style>
            #calendar {
                max-width: 100%;
            }

            .col-centered {
                float: none;
                margin: 0 auto;
            }

            .fc-title {
                color: #f8f9fa;
            }

            /* .scrolll {
                border: 1px solid white;
                width: 1000px;
                height: 100%;
                overflow-y: hidden;
                overflow-x: scroll;
            } */
        </style>

    </head>

    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="../index.php" class="nav-link">Beranda</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="../app/logout.php" class="nav-link">Keluar</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="../app/logout.php" class="nav-link" style="color: indianred;">Login sebagai <?php echo $level; ?></a>
                    </li>
                </ul>

                <!-- SEARCH FORM -->
                <!-- Right navbar links -->
            </nav>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="../public/index3.html" class="brand-link" style="background-color: #a2ad65;">
                    <img src="../public/image/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="height: 100px; width: 60px; opacity: .8;">
                    <span class="brand-text font-weight-light">Telkom Akses</span>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <?php if ($_SESSION['foto'] == '-') { ?>
                                <img src="../public/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">

                            <?php  } else { ?>
                                <img src="foto_user/<?php echo $_SESSION['foto']; ?>" class="img-circle elevation-2" alt="User Image" style="width: 40px;height: 40px;">
                            <?php  } ?>
                        </div>
                        <div class="info">
                            <a href="#" class="d-block"><?php echo $nama_user; ?></a>
                        </div>
                    </div>

                    <!-- SidebarSearch Form -->
                    <!-- Sidebar Menu -->
                    <?php include_once 'view/menu.php'; ?>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                </section>

                <!-- Main content -->
                <section class="content">

                    <!-- Default box -->
                    <?php include_once 'controller/loading.php'; ?>
                    <!-- /.card -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="float-right d-none d-sm-block">
                    <b>Version</b> 3.1.0-pre
                </div>
                <strong>Copyright &copy; 2014-2020 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <!-- jQuery -->
        <script src="../public/plugins/jquery/jquery.min.js"></script>
        <script src="../public/js/jquery.js"></script>

        <!-- Bootstrap 4 -->
        <script src="../public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Select2 -->
        <script src="../public/plugins/select2/js/select2.full.min.js"></script>
        <!-- SweetAlert2 -->
        <script src="../public/plugins/sweetalert2/sweetalert2.min.js"></script>
        <!-- Toastr -->
        <script src="../public/plugins/toastr/toastr.min.js"></script>
        <!-- DataTables -->
        <script src="../public/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="../public/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="../public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
        <!-- AdminLTE App -->
        <script src="../public/dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../public/dist/js/demo.js"></script>
        <!-- fullcalendar -->
        <script src='../public/js/moment.min.js'></script>
        <script src="../public/js/fullcalendar.min.js"></script>
        <script>
            $(function() {
                $("#example1").DataTable({
                    // "responsive": true,
                    "autoWidth": true,
                    scrollX: true,
                    processing: true,
                    scrollY: "400px",
                    scrollCollapse: true,
                    paging: false,
                    bDestroy: true,
                });
                $("#example3").DataTable({
                    // "responsive": true,
                    "autoWidth": true,
                    scrollX: true,
                    processing: true,
                    scrollY: "400px",
                    scrollCollapse: true,
                    paging: false,
                    bDestroy: true,
                });
                $('.dataTables_length').addClass('bs-select');
                $("#example2").DataTable({});
            });
            $(function() {
                //Initialize Select2 Elements
                $('.select2').select2();
            });

            $('.edit_karyawan_kan').click(function() {
                var $row = $(this).closest("tr"),
                    $kolom2 = $row.find("td:nth-child(2)");
                $kolom3 = $row.find("td:nth-child(3)");
                $kolom4 = $row.find("td:nth-child(4)");
                $kolom5 = $row.find("td:nth-child(5)");
                $kolom6 = $row.find("td:nth-child(6)");
                $('#idkaryawan_edit').val($kolom2.text());
                $('#namakaryawan_edit').val($kolom3.text());
                $("#jeniskelamin_edit").val($kolom4.text()).change();
                $("#agamakarywan_id").val($kolom5.text()).change();
                $('#job_edit').val($kolom6.text());

                console.log($kolom2.text());
            });

            $('.edit_trainier_kan').click(function() {
                var $row = $(this).closest("tr"),
                    $kolom2 = $row.find("td:nth-child(2)");
                $kolom3 = $row.find("td:nth-child(3)");
                $kolom4 = $row.find("td:nth-child(4)");


                $('#idtrainer_edit').val($kolom2.text());
                $('#namatrainer_edit').val($kolom3.text());
                $("#jeniskelamin_edit").val($kolom4.text()).change();


            });

            $('.edit_user_kan').click(function() {
                var $row = $(this).closest("tr"),
                    $kolom2 = $row.find("td:nth-child(2)");
                $kolom3 = $row.find("td:nth-child(3)");
                $kolom4 = $row.find("td:nth-child(4)");
                $kolom5 = $row.find("td:nth-child(5)");
                $kolom6 = $row.find("td:nth-child(6)");

                $('#id_user').val($kolom2.text());
                $('#nama_user').val($kolom3.text());
                $('#username').val($kolom4.text());
                $('#passowrd').val($kolom5.text());
                $('#level').val($kolom6.text());

            });
        </script>
        <?php

        function tanggal_sekarang($time = FALSE)
        {
            date_default_timezone_set('Asia/Jakarta');
            $str_format = '';
            if ($time == FALSE) {
                $str_format = date("Y-m-d");
            } else {
                $str_format = date("Y-m-d H:i:s");
            }
            return $str_format;
        }
        ?>

        <script>
            $(document).ready(function() {
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,basicWeek,basicDay'
                    },
                    defaultDate: '<?= tanggal_sekarang(); ?>',
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    selectable: true,
                    selectHelper: true,
                    select: function(start, end) {

                        $('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
                        $('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
                        $('#ModalAdd').modal('show');
                    },
                    eventRender: function(event, element) {
                        element.bind('dblclick', function() {
                            $('#ModalEdit #id_even_edit').val(event.id_even);
                            $('#ModalEdit #title_even_edit').val(event.title);
                            $('#ModalEdit #jumlah_peserta_edit').val(event.jumlah_peserta);
                            $('#ModalEdit #lokasi_edit').val(event.lokasi);
                            $('#ModalEdit #periode_edit').val(event.periode);
                            $('#ModalEdit #trainer_edit').val(event.id_trainer).change();
                            $("#status_edit").val(event.color).change();
                            $('#ModalEdit').modal('show');
                            console.log(event.color);

                        });
                    },
                    eventDrop: function(event, delta, revertFunc) { // si changement de position

                        edit(event);

                    },
                    eventResize: function(event, dayDelta, minuteDelta, revertFunc) { // si changement de longueur

                        edit(event);

                    },
                    events: [
                        <?php foreach ($events as $event) :

                            $start = explode(" ", $event['start']);
                            $end = explode(" ", $event['end']);
                            if ($start[1] == '00:00:00') {
                                $start = $start[0];
                            } else {
                                $start = $event['start'];
                            }
                            if ($end[1] == '00:00:00') {
                                $end = $end[0];
                            } else {
                                $end = $event['end'];
                            }
                        ?> {
                                id_even: '<?php echo $event['id_even']; ?>',
                                title: '<?php echo $event['title']; ?>',
                                start: '<?php echo $start; ?>',
                                end: '<?php echo $end; ?>',
                                color: '<?php echo $event['color']; ?>',
                                jumlah_peserta: '<?php echo $event['jumlah_peserta']; ?>',
                                lokasi: '<?php echo $event['lokasi']; ?>',
                                periode: '<?php echo $event['periode']; ?>',
                                id_trainer: '<?php echo $event['id_trainer']; ?>',

                            },
                        <?php endforeach; ?>
                    ]
                });

                function edit(event) {
                    start = event.start.format('YYYY-MM-DD HH:mm:ss');
                    if (event.end) {
                        end = event.end.format('YYYY-MM-DD HH:mm:ss');
                    } else {
                        end = start;
                    }

                    id_even = event.id_even;

                    Event = [];
                    Event[0] = id_even;
                    Event[1] = start;
                    Event[2] = end;

                    $.ajax({
                        url: 'controller/editEventDate.php',
                        type: "POST",
                        data: {
                            Event: Event
                        },
                        success: function(rep) {
                            if (rep == 'OK') {
                                alert('Simpan Even...');
                            } else {
                                alert('Anda telah memindahkan jadwal pelatihan.....');
                            }
                        }
                    });
                }

            });

            // action edit karyawan
        </script>

    </body>

    </html>
<?php } ?>