<?php
include '../../app/koneksi.php';
$select = mysqli_query($connect, "SELECT tbl_even.*, tbl_trainer.* FROM tbl_even
                        LEFT JOIN tbl_trainer on tbl_even.id_trainer=tbl_trainer.id_trainer
                       where id_even =$_GET[id_iven]");
foreach ($select as $data) {
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>

    <body onload="window.print()">
        <!-- onload="window.print()" -->
        <table border="0" style="width: 100%;">
            <tr>
                <td colspan="7" align="center">
                    <b>
                        <h2>PT. TELKOM AKSES</h2>
                    </b>
                    <div>
                        <h4>Laporan Data Event</h4>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Judul Ivent</td>
                <td>:</td>
                <td><?php echo $data['title']; ?></td>
                <td></td>
                <td>Periode</td>
                <td>:</td>
                <td><?php echo $data['periode']; ?></td>
            </tr>
            <tr>
                <td>Trainer</td>
                <td>:</td>
                <td><?php echo $data['nama_trainer']; ?></td>
                <td></td>
                <td>Jumlah Peserta</td>
                <td>:</td>
                <td><?php echo $data['jumlah_peserta']; ?></td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td>:</td>
                <td><?php echo date('d-m-Y', strtotime($data['start'])); ?></td>
                <td></td>
                <td>Lokasi</td>
                <td>:</td>
                <td><?php echo $data['lokasi']; ?></td>
            </tr>
            </tr>
        </table>
        <br><br>
        <div class="row">
            <div class="card-body">
                <table border="1" style="width: 100%;border-collapse: collapse;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID</th>
                            <th>ID Karyawan</th>
                            <th>Nama_Peserta</th>
                            <th>Jenis Kelamin</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        $tampilkansaja = mysqli_query($connect, "SELECT * FROM tbl_peserta
                            INNER JOIN tbl_karyawan ON tbl_peserta.id_karyawan=tbl_karyawan.id_karyawan
                            where tbl_peserta.id_even='$_GET[id_iven]' order by tbl_peserta.id_peserta desc");
                        foreach ($tampilkansaja as $rowdatakan) {
                        ?>
                            <tr align="center">
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $rowdatakan['id_peserta']; ?></td>
                                <td><?php echo $rowdatakan['id_karyawan']; ?></td>
                                <td><?php echo $rowdatakan['nama_karyawan']; ?></td>
                                <td><?php echo $rowdatakan['jenis_kelamin']; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <br><br><br><br>
        <br><br><br><br>
        <br><br>
        <table border="0" align="right">
            <tr>
                <td align="center">Padang, <?php echo date('d-m-Y');?></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td align="center">
                <br>
                <br>
                <br><br><br>    
                Panitia</td>
            </tr>
        </table>
    </body>

    </html>

<?php } ?>