<?php
$no = 1;
$select = mysqli_query($connect, "SELECT tbl_even.*, tbl_trainer.* FROM tbl_even
                        LEFT JOIN tbl_trainer on tbl_even.id_trainer=tbl_trainer.id_trainer
                       where id_even =$_GET[id]");
foreach ($select as $data) {
?>
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">Detail Event</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>TITLE EVENT</label>
                                            <textarea class="form-control" rows="3" readonly><?php echo $data['title']; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tanggal</label>
                                            <input type="text" class="form-control" readonly value="<?php echo date("Y/m/d", strtotime($data['start'])) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Jumlah Peserta</label>
                                            <input type="text" class="form-control" readonly value="<?php echo $data['jumlah_peserta'] ?> Orang">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">TAMBAH PESERTA</h3>
                        </div>

                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="controller/addPeserta.php" method="POST">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="hidden" name="id_even" value="<?php echo $_GET['id']; ?>">
                                            <label>Pilih Peserta</label>
                                            <select class="form-control select2" style="width: 100%;" name="id_karyawan">
                                                <option value="0">-- Pilih --</option>
                                                <?php
                                                $select_peserta = mysqli_query($connect, "SELECT * FROM tbl_karyawan");
                                                foreach ($select_peserta as $peserta) {
                                                ?>
                                                    <option value="<?php echo $peserta['id_karyawan'] ?>"><?php echo $peserta['id_karyawan']; ?> | <?php echo $peserta['nama_karyawan']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="">.</label>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-info">Tambah Peserta</button>
                                        </div>
                                        <a target="_blank" href="view/print_laporan_peserta.php?id_iven=<?php echo $_GET['id']; ?>" class="btn btn-primary">
                                            PRINT LAPORAN
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="card-body">
                    <table id="example2" class="table table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID</th>
                                <th>ID Karyawan</th>
                                <th>Nama_Peserta</th>
                                <th>Jenis Kelamin</th>
                                <th style="width: 130px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkansaja = mysqli_query($connect, "SELECT * FROM tbl_peserta
                            INNER JOIN tbl_karyawan ON tbl_peserta.id_karyawan=tbl_karyawan.id_karyawan
                            where tbl_peserta.id_even='$_GET[id]' order by tbl_peserta.id_peserta desc");
                            foreach ($tampilkansaja as $rowdatakan) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $rowdatakan['id_peserta']; ?></td>
                                    <td><?php echo $rowdatakan['id_karyawan']; ?></td>
                                    <td><?php echo $rowdatakan['nama_karyawan']; ?></td>
                                    <td><?php echo $rowdatakan['jenis_kelamin']; ?></td>
                                    <td>
                                        <a href="controller/hapus_peserta.php?id=<?php echo $rowdatakan['id_peserta']; ?>&id_ivend=<?php echo $rowdatakan['id_even']; ?>" class="btn btn-danger btn-sm" onClick="return confirm('Apa anda yakin?')">Delete</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } ?>