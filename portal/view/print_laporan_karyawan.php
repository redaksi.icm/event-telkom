<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body onload="window.print()">
    <table border="0" style="width: 100%;">
        <tr>
            <td colspan="7" align="center">
                <b>
                    <h2>PT. TELKOM AKSES</h2>
                </b>
                <div>
                    <h4>Laporan Data Pegawai</h4>
                </div>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <br>
    <div class="card">
        <table border="1" style="width: 100%;border-collapse: collapse;">
            <thead>
                <tr>
                    <th>No</th>
                    <th>ID Karyawan</th>
                    <th>Nama Karyawan</th>
                    <th>Jenis Kelamin</th>
                    <th>Agama</th>
                    <th>Job Desk</th>
                    <th>ID Akun</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                include '../../app/koneksi.php';
                $select = mysqli_query($connect, "SELECT * FROM tbl_karyawan order by nama_karyawan desc");
                foreach ($select as $data) {
                ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $data['id_karyawan']; ?></td>
                        <td><?php echo $data['nama_karyawan']; ?></td>
                        <td><?php echo $data['jenis_kelamin']; ?></td>
                        <td><?php echo $data['agama']; ?></td>
                        <td><?php echo $data['job_desk']; ?></td>
                        <td><?php echo $data['id_akun']; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

        <br>
        <br><br><br><br>
        <br><br><br><br>
        <br><br>
        <table border="0" align="right">
            <tr>
                <td align="center">Padang, <?php echo date('d-m-Y');?></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td align="center">
                <br>
                <br>
                <br><br><br>    
                Panitia</td>
            </tr>
        </table>

</body>

</html>