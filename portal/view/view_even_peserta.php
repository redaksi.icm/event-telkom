<div class="card">
    <div class="card-header">
        <h3 class="card-title">DAFTAR SCHEDULE EVENT </h3>
        <br>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table id="example1" class="table table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID</th>
                                <th>Tanggal</th>
                                <th style="width: 200px;">Title</th>
                                <!-- <th>Trainer</th> -->
                                <th>Lokasi</th>
                                <th style="width: 100px;">Periode</th>
                                <th>Modul</th>
                                <th>Status</th>
                                <th>Status_Peserta</th>
                                <!-- <th>Jumlah_Ikut</th> -->
                                <th style="width: 130px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // echo $_SESSION['id_akun'];
                            $no = 1;
                            $select = mysqli_query($connect, "SELECT tbl_even.*, tbl_trainer.*, tbl_peserta.id_peserta, tbl_peserta.status_peserta FROM tbl_even
                        INNER JOIN tbl_trainer on tbl_even.id_trainer=tbl_trainer.id_trainer
                        INNER JOIN tbl_peserta on tbl_even.id_even=tbl_peserta.id_even
                        where status !='Closed' and id_karyawan = '$_SESSION[id_akun]' and status_peserta !='Diikuti'
                        order by id_even desc");
                            foreach ($select as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['id_even'] ?></td>
                                    <td><?php echo date("Y/m/d", strtotime($data['start'])); ?></td>
                                    <td><?php echo $data['title']; ?></td>
                                    <td><?php echo $data['lokasi']; ?></td>
                                    <td><?php echo $data['periode']; ?></td>
                                    <td>
                                        <?php if ($data['modul'] == '0') { ?>
                                            Modul Belum Ada
                                        <?php } else { ?>
                                            <a href="modul/<?php echo $data['modul']; ?>">Download</a>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ($data['status'] == 'Open') { ?>
                                            <a class="btn btn-success btn-sm">
                                                <?php echo $data['status']; ?>
                                            </a>
                                        <?php } elseif ($data['status'] == 'Proses') { ?>
                                            <a class="btn btn-primary btn-sm">
                                                <?php echo $data['status']; ?>
                                            </a>
                                        <?php } else { ?>
                                            <a class="btn btn-danger btn-sm">
                                                <?php echo $data['status']; ?>
                                            </a>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ($data['status_peserta'] == 'Diikuti') { ?>
                                            <a class="btn btn-primary btn-sm" style="color: white;">
                                                Sudah di dikuti
                                            </a>
                                        <?php } else { ?>
                                            <a class="btn btn-danger btn-sm" style="color: white;">
                                                Belum di ikuti
                                            </a>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <div class="timeline-footer">
                                            <a href="controller/action_ikuti_event_peserta.php?&id=<?php echo $data['id_peserta']; ?>" class="btn btn-primary btn-sm" onclick="return confirm('Apa anda yakin?')">
                                                <i class="fas fa-clock"></i> Ikuti event
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>