<div class="card">
    <div class="card-header">
        <h3 class="card-title">DAFTAR EVENT DI IKUTI </h3>
        <br>
        <div class="row">
            <div class="col-12">
                <!-- <div class="card"> -->
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID</th>
                            <th>Tanggal</th>
                            <th style="width: 200px;">Title</th>
                            <!-- <th>Trainer</th> -->
                            <th>Lokasi</th>
                            <th style="width: 100px;">Periode</th>
                            <th>Status</th>
                            <th>Status_Peserta</th>
                            <th>Sertifikat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        $select = mysqli_query($connect, "SELECT tbl_even.*, tbl_trainer.*, tbl_peserta.id_peserta, tbl_peserta.status_peserta, tbl_peserta.nilai_sertifikat FROM tbl_even
                        INNER JOIN tbl_trainer on tbl_even.id_trainer=tbl_trainer.id_trainer
                        INNER JOIN tbl_peserta on tbl_even.id_even=tbl_peserta.id_even
                        where id_karyawan = '$_SESSION[id_akun]' and status_peserta ='Diikuti'
                        order by id_even desc");
                        foreach ($select as $data) {
                        ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $data['id_even'] ?></td>
                                <td><?php echo date("Y/m/d", strtotime($data['start'])); ?></td>
                                <td><?php echo $data['title']; ?></td>
                                <td><?php echo $data['lokasi']; ?></td>
                                <td><?php echo $data['periode']; ?></td>
                                <td>
                                    <?php if ($data['status'] == 'Open') { ?>
                                        <a class="btn btn-success btn-sm">
                                            <?php echo $data['status']; ?>
                                        </a>
                                    <?php } elseif ($data['status'] == 'Proses') { ?>
                                        <a class="btn btn-primary btn-sm">
                                            <?php echo $data['status']; ?>
                                        </a>
                                    <?php } else { ?>
                                        <a class="btn btn-danger btn-sm">
                                            <?php echo $data['status']; ?>
                                        </a>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php if ($data['status_peserta'] == 'Diikuti') { ?>
                                        <a class="btn btn-primary btn-sm" style="color: white;">
                                            Sudah di dikuti
                                        </a>
                                    <?php } else { ?>
                                        <a class="btn btn-danger btn-sm" style="color: white;">
                                            Belum di ikuti
                                        </a>
                                    <?php } ?>
                                </td>
                                <td>
                                <?php if ($data['nilai_sertifikat'] == '0') { ?>
                                            Belum di upload
                                        <?php } else { ?>
                                            <a target="_blank" href="view/print_sertifikat.php?id_even=<?php echo $data['id_even']; ?>">Print</a>
                                        <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>