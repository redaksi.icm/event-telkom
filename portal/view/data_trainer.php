<div class="card">
    <div class="card-header">
        <h3 class="card-title">DATA TRAINER </h3>
        <br>
        <br>
        <div class="row">
            <div class="col-12">
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-default">
                    TAMBAH DATA
                </button>
                <br>
                <br>
                <div class="card">
                    <table id="example2" class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID Tainer</th>
                                <th>Nama Trainer</th>
                                <th>Jenis Kelamin</th>
                                <th>ID Akun</th>
                                <th style="width: 130px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $select = mysqli_query($connect, "SELECT * FROM tbl_trainer order by id_trainer desc");
                            foreach ($select as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['id_trainer']; ?></td>
                                    <td><?php echo $data['nama_trainer']; ?></td>
                                    <td><?php echo $data['jenis_kelamin']; ?></td>
                                    <td><?php echo $data['id_akun']; ?></td>
                                    <td>
                                        <div class="timeline-footer">
                                            <a href="controller/buat_akun_trainer.php?id=<?php echo $data['id_trainer']; ?>&id_akun=<?php echo $data['id_akun']; ?>" class="btn btn-primary btn-sm">
                                                Buat Akun
                                            </a>
                                            <a href="#" class="btn btn-danger btn-sm edit_trainier_kan" data-toggle="modal" data-target="#modal-default_edit">Edit</a>
                                            <a href="controller/hapus_trainer.php?id=<?php echo $data['id_trainer']; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apa anda yakin?')">Delete</a>


                                        </div>
                                    </td>
                                </tr>

                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal -->

<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah data trainer</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="controller/simpan_trainer.php">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>ID Trainer/ Identitas</label>
                                    <input type="text" class="form-control" name="idtrainer">
                                </div>
                                <div class="form-group">
                                    <label>Nama Trainer</label>
                                    <input type="text" class="form-control" name="namatrainer">
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <select class="form-control select2" style="width: 100%;" name="jeniskelamin">
                                        <option value="0">-- Pilih --</option>
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- edit -->
<div class="modal fade" id="modal-default_edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit data trainer</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="controller/simpan_edit_trainer.php">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>ID Trainer/ Identitas</label>
                                    <input type="text" class="form-control" name="idtrainer_edit" id="idtrainer_edit" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Nama Trainer</label>
                                    <input type="text" class="form-control" name="namatrainer_edit" id="namatrainer_edit">
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <select class="form-control select2" style="width: 100%;" name="jeniskelamin_edit" id="jeniskelamin_edit">
                                        <option value="0">-- Pilih --</option>
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>