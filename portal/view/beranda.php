<?php
require_once('../app/bdd.php');
$sql = "SELECT * FROM tbl_even";
$req = $bdd->prepare($sql);
$req->execute();
$events = $req->fetchAll();
?>
<?php if ($_SESSION['level'] == 'Admin') { ?>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
                <span class="info-box-icon bg-danger"><i class="fa fa-calendar-plus"></i></span>
                <?php
                include "../app/koneksi.php";
                $sql = mysqli_query($connect, "SELECT status FROM tbl_even where status='Open'");
                $jumlah = mysqli_num_rows($sql);
                ?>
                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Event (open)</span>
                    <span class="info-box-number"><?php echo $jumlah; ?></span>
                    <!-- <span class="info-box-text"><a href="">Lihat Even</a></span> -->
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <div class="col-sm-4 col-md-2">
            <h4 class="text-center">KETERANGAN</h4>
            <div class="color-palette-set">
                <div class="bg-Success color-palette"><span>Open Event</span></div>
            </div>
        </div>
        <div class="col-sm-4 col-md-2">
            <h4 class="text-center">.</h4>
            <div class="color-palette-set">
                <div class="bg-info color-palette"><span>On progres dan Finish</span></div>
            </div>
        </div>
        <div class="col-sm-4 col-md-2">
            <h4 class="text-center">.</h4>
            <div class="color-palette-set">
                <div class="bg-Danger color-palette"><span>Closed</span></div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">DAFTAR SCHEDULE EVENT </h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div id="calendar" class="col-centered">
                    </div>
                </div>

            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">

        </div>
        <!-- /.card-footer-->
    </div>

    <!-- modal -->
    <div class="modal fade" id="ModalAdd">
        <div class="modal-dialog">
            <form action="controller/addEvent.php" method="POST">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Even</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group" style="width: 100%;">
                                <label>Title Even</label>
                                <input type="text" class="form-control" id="title_even" name="title_even" placeholder="Title Even">
                            </div>
                            <div class="form-group" style="width: 100%;">
                                <label>Trainer</label>
                                <select class="form-control select2" style="width: 100%;" name="trainer" id="trainer">
                                    <option value="0">-- Pilih --</option>
                                    <?php
                                    $tampil = mysqli_query($connect, "SELECT * FROM tbl_trainer");
                                    foreach ($tampil as $datakan) {
                                    ?>
                                        <option value="<?php echo $datakan['id_trainer']; ?>"><?php echo $datakan['nama_trainer']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Lokasi</label>
                                <input type="text" class="form-control" id="lokasi" name="lokasi" placeholder="Lokasi">
                            </div>
                            <div class="form-group" style="padding-left: 30px;">
                                <label>Periode</label>
                                <input type="text" class="form-control" id="periode" name="periode" placeholder="Periode">
                            </div>
                            <div class="form-group">
                                <label>Jumlah Peserta</label>
                                <input type="number" class="form-control" id="jumlah_peserta" name="jumlah_peserta" placeholder="Jumlah Peserta">
                            </div>
                            <div class="form-group" style="padding-left: 30px;">
                                <label>Status</label>
                                <select class="form-control" style="width: 100%;" name="status">
                                    <option selected="selected">-- Pilih --</option>
                                    <option value="#008000">Open Event</option>
                                    <option value="#0071c5">On progres dan Finish</option>
                                    <option value="#FF0000">Closed</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <input type="hidden" class="form-control" id="start" name="start">
                            <input type="hidden" class="form-control" id="end" name="end">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="simpan" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- edit -->
    <div class="modal fade" id="ModalEdit">
        <div class="modal-dialog">
            <form action="controller/UpdateEvent.php" method="POST">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Even</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group" style="width: 100%;">
                                <label>Title Even</label>
                                <input type="text" class="form-control" id="title_even_edit" name="title_even" placeholder="Title Even">
                                <input type="hidden" class="form-control" id="id_even_edit" name="id_even" placeholder="Title Even">
                            </div>
                            <div class="form-group" style="width: 100%;">
                                <label>Trainer</label>
                                <select class="form-control select2" style="width: 100%;" name="trainer" id="trainer_edit">
                                    <option value="0">-- Pilih --</option>
                                    <?php
                                    $tampil = mysqli_query($connect, "SELECT * FROM tbl_trainer");
                                    foreach ($tampil as $datakan) {
                                    ?>
                                        <option value="<?php echo $datakan['id_trainer']; ?>"><?php echo $datakan['nama_trainer']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Lokasi</label>
                                <input type="text" class="form-control" id="lokasi_edit" name="lokasi" placeholder="Lokasi">
                            </div>
                            <div class="form-group" style="padding-left: 30px;">
                                <label>Periode</label>
                                <input type="text" class="form-control" id="periode_edit" name="periode" placeholder="Periode">
                            </div>
                            <div class="form-group">
                                <label>Jumlah Peserta</label>
                                <input type="text" class="form-control" id="jumlah_peserta_edit" name="jumlah_peserta" placeholder="Jumlah Peserta">
                            </div>
                            <div class="form-group" style="padding-left: 30px;">
                                <label>Status</label>
                                <select class="form-control" style="width: 100%;" id="status_edit" name="status">
                                    <option selected="selected">-- Pilih --</option>
                                    <option value="#008000">Open Event</option>
                                    <option value="#0071c5">On progres dan Finish</option>
                                    <option value="#FF0000">Closed</option>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="row">
                        <input type="text" class="form-control" id="start_edit" name="start">
                        <input type="text" class="form-control" id="end_edit" name="end">
                    </div> -->
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="simpan" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php } elseif ($_SESSION['level'] == 'Trainer') { ?>
    <?php include_once 'beranda_trainer.php'; ?>

<?php } elseif ($_SESSION['level'] == 'Peserta') { ?>
    <?php include_once 'beranda_peserta.php'; ?>
<?php } ?>