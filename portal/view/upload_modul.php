<div class="card">
    <div class="card-header">
        <h3 class="card-title">Upload Modul</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
            Input Modul
        </button>
    </div>
    <div class="card-body">
        <!-- <div class="row"> -->
        <table id="example2" class="table table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>ID</th>
                    <th>Tanggal</th>
                    <th style="width: 200px;">Title</th>
                    <th style="width: 200px;">Modul</th>
                    <th style="width: 130px;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                // echo $_SESSION['id_user'];
                // $session_trainer =  mysqli_query($connect, "SELECT * FROM tbl_trainer where id_akun ");
                $no = 1;
                $select = mysqli_query($connect, "SELECT tbl_even.*, tbl_trainer.* FROM tbl_even
                        INNER JOIN tbl_trainer on tbl_even.id_trainer=tbl_trainer.id_trainer 
                        where id_akun = '$_SESSION[id_akun]'
                        and modul != '0'
                        order by id_even desc");
                foreach ($select as $data) {
                ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $data['id_even']; ?></td>
                        <td><?php echo date("Y/m/d", strtotime($data['start'])); ?></td>
                        <td><?php echo $data['title']; ?></td>
                        <td> <a href="modul/<?php echo $data['modul']; ?>">Download</a></td>
                        <td>
                            <a href="controller/hapus_modul.php?id=<?php echo $data['id_even']; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apa anda yakin?')">Delete</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <!-- </div> -->
    </div>
</div>
<!-- batas -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload Modul</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="controller/action_upload_modul.php" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="hidden" name="id_even" value="<?php echo $_GET['id']; ?>">
                                <label>Title Enevt</label>
                                <select class="form-control select2" style="width: 100%;" name="nama_event">
                                    <option value="0">-- Pilih --</option>
                                    <?php
                                    $select_peserta = mysqli_query($connect, "SELECT tbl_even.*, tbl_trainer.* FROM tbl_even
                                    INNER JOIN tbl_trainer on tbl_even.id_trainer=tbl_trainer.id_trainer 
                                    where id_akun = '$_SESSION[id_akun]' and status !='Closed' and modul = '0'
                                    order by id_even desc");
                                    foreach ($select_peserta as $peserta) {
                                    ?>
                                        <option value="<?php echo $peserta['id_even'] ?>"><?php echo $peserta['title']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Modul</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="modul">
                                        <label class="custom-file-label" for="exampleInputFile">Pilih Modul</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Upload</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Upload</button>
            </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>