<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="index.php" class="nav-link">
                <i class="nav-icon fa fa-home"></i>
                <p>
                    Beranda
                </p>
            </a>
        </li>
        <?php if ($_SESSION['level'] == 'Admin') { ?>
            <li class="nav-item">
                <a href="index.php?act=2" class="nav-link">
                    <i class="nav-icon fa fa-calendar-check"></i>
                    <p>
                        Data Even
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="index.php?act=10" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        Data Pegawai
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="index.php?act=11" class="nav-link">
                    <i class="nav-icon fas fa-user"></i>
                    <p>
                        Data Trainer
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="index.php?act=12" class="nav-link">
                    <i class="nav-icon fas fa-cog"></i>
                    <p>
                        Data User
                    </p>
                </a>
            </li>
        <?php } elseif ($_SESSION['level'] == 'Trainer') { ?>
            <li class="nav-item">
                <a href="index.php?act=7" class="nav-link">
                    <i class="nav-icon fas fa-copy"></i>
                    <p>
                        Event
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="index.php?act=5" class="nav-link">
                    <i class="nav-icon fas fa-copy"></i>
                    <p>
                        Upload Modul
                    </p>
                </a>
            </li>
        <?php } else { ?>
            <li class="nav-item">
                <a href="index.php?act=6" class="nav-link">
                    <i class="nav-icon fas fa-copy"></i>
                    <p>
                        Schedule Event
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="index.php?act=8" class="nav-link">
                    <i class="nav-icon fas fa-clock"></i>
                    <p>
                        Event Selesai
                    </p>
                </a>
            </li>
        <?php } ?>
    </ul>
</nav>