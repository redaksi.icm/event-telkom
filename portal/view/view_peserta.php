<?php
 $even = mysqli_query($connect, "SELECT * FROM tbl_even where id_even='$_GET[id]'");
 foreach ($even as $even){
     $nama_even = $even['title'];
 }
?>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">DAFTAR PESERTA EVENT <b style="color: red;"><?php echo $nama_even;?></b></h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="card-body">
            <table id="example2" class="table table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>ID</th>
                        <th>ID Karyawan</th>
                        <th>Nama_Peserta</th>
                        <th>Jenis Kelamin</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    $tampilkansaja = mysqli_query($connect, "SELECT * FROM tbl_peserta
                            INNER JOIN tbl_karyawan ON tbl_peserta.id_karyawan=tbl_karyawan.id_karyawan
                            where tbl_peserta.id_even='$_GET[id]' order by tbl_peserta.id_peserta desc");
                    foreach ($tampilkansaja as $rowdatakan) {
                    ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $rowdatakan['id_peserta']; ?></td>
                            <td><?php echo $rowdatakan['id_karyawan']; ?></td>
                            <td><?php echo $rowdatakan['nama_karyawan']; ?></td>
                            <td><?php echo $rowdatakan['jenis_kelamin']; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
