<div class="card">
    <div class="card-header">
        <h3 class="card-title">DAFTAR SCHEDULE EVENT </h3>
        <br>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table id="example3" class="table table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID</th>
                                <th>Tanggal</th>
                                <th style="width: 200px;">Title</th>
                                <th>Trainer</th>
                                <th>Lokasi</th>
                                <th style="width: 100px;">Periode</th>
                                <th>Status</th>
                                <th>Jumlah_Peserta</th>
                                <!-- <th>Jumlah_Ikut</th> -->
                                <th style="width: 130px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // echo $_SESSION['id_user'];
                            // $session_trainer =  mysqli_query($connect, "SELECT * FROM tbl_trainer where id_akun ");
                            $no = 1;
                            $select = mysqli_query($connect, "SELECT tbl_even.*, tbl_trainer.* FROM tbl_even
                        INNER JOIN tbl_trainer on tbl_even.id_trainer=tbl_trainer.id_trainer 
                        where id_akun = '$_SESSION[id_akun]'
                        order by id_even desc");
                            foreach ($select as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['id_even']; ?></td>
                                    <td><?php echo date("Y/m/d", strtotime($data['start'])); ?></td>
                                    <td><?php echo $data['title']; ?></td>
                                    <td><?php echo $data['nama_trainer']; ?></td>
                                    <td><?php echo $data['lokasi']; ?></td>
                                    <td><?php echo $data['periode']; ?></td>
                                    <td>
                                        <?php if ($data['status'] == 'Open') { ?>
                                            <a class="btn btn-success btn-sm">
                                                <?php echo $data['status']; ?>
                                            </a>
                                        <?php } elseif ($data['status'] == 'Proses') { ?>
                                            <a class="btn btn-primary btn-sm">
                                                <?php echo $data['status']; ?>
                                            </a>
                                        <?php } else { ?>
                                            <a class="btn btn-danger btn-sm">
                                                <?php echo $data['status']; ?>
                                            </a>
                                        <?php } ?>
                                    </td>
                                    <td><?php echo $data['jumlah_peserta']; ?>, Orang</td>
                                    <!-- <td>X, Orang</td> -->
                                    <td>
                                        <div class="timeline-footer">
                                            <?php if ($data['status'] == 'Closed') { ?>
                                                <a href="index.php?act=13&id=<?php echo $data['id_even']; ?>" class="btn btn-danger btn-sm">
                                                    <i class="fas fa-users"></i> Input Nilai
                                                </a>
                                            <?php } else { ?>
                                                <a href="index.php?act=4&id=<?php echo $data['id_even']; ?>" class="btn btn-primary btn-sm">
                                                    <i class="fas fa-users"></i> View
                                                </a>
                                            <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>