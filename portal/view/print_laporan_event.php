<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body onload="window.print()">
    <!-- onload="window.print()" -->
    <table border="0" style="width: 100%;">
        <tr>
            <td colspan="7" align="center">
                <b>
                    <h2>PT. TELKOM AKSES</h2>
                </b>
                <div>
                    <h4>Laporan Data Event</h4>
                </div>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <br>

    <div class="card">
        <table border="1" style="width: 100%;border-collapse: collapse;">
            <thead>
                <tr>
                    <th>No</th>
                    <th>ID</th>
                    <th>Tanggal</th>
                    <th style="width: 200px;">Title</th>
                    <th>Trainer</th>
                    <th>Lokasi</th>
                    <th style="width: 100px;">Periode</th>
                    <th>Status</th>
                    <th>Jumlah_Peserta</th>
                </tr>
            </thead>
            <tbody align="center">
                <?php
                $no = 1;
                include '../../app/koneksi.php';
                $select = mysqli_query($connect, "SELECT tbl_even.*, tbl_trainer.* FROM tbl_even
                        LEFT JOIN tbl_trainer on tbl_even.id_trainer=tbl_trainer.id_trainer
                        order by id_even desc");
                foreach ($select as $data) {
                ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $data['id_even']; ?></td>
                        <td><?php echo date("Y/m/d", strtotime($data['start'])); ?></td>
                        <td><?php echo $data['title']; ?></td>
                        <td><?php echo $data['nama_trainer']; ?></td>
                        <td><?php echo $data['lokasi']; ?></td>
                        <td><?php echo $data['periode']; ?></td>
                        <td>
                            <?php if ($data['status'] == 'Open') { ?>
                                <a class="btn btn-success btn-sm">
                                    <?php echo $data['status']; ?>
                                </a>
                            <?php } elseif ($data['status'] == 'Proses') { ?>
                                <a class="btn btn-primary btn-sm">
                                    <?php echo $data['status']; ?>
                                </a>
                            <?php } else { ?>
                                <a class="btn btn-danger btn-sm">
                                    <?php echo $data['status']; ?>
                                </a>
                            <?php } ?>
                        </td>
                        <td><?php echo $data['jumlah_peserta']; ?>, Orang</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

    <br>
    <br><br><br><br>
    <br><br><br><br>
    <br><br>
    <table border="0" align="right">
        <tr>
            <td align="center">Padang, <?php echo date('d-m-Y'); ?></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td align="center">
                <br>
                <br>
                <br><br><br>
                Panitia</td>
        </tr>
    </table>

</body>

</html>