<div class="card">
    <div class="card-header">
        <h3 class="card-title">DATA KARYAWAN </h3>
        <br>
        <br>
        <div class="row">
            <div class="col-12">
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-default">
                    TAMBAH DATA
                </button>
                <a target="_blank" href="view/print_laporan_karyawan.php" class="btn btn-success">
                   PRINT LAPORAN
                </a>
                <div class="card">
                    <table id="example1" class="table table-hover text-nowrap">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID Karyawan</th>
                                <th>Nama Karyawan</th>
                                <th>Jenis Kelamin</th>
                                <th>Agama</th>
                                <th>Job Desk</th>
                                <th>ID Akun</th>
                                <th style="width: 130px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $select = mysqli_query($connect, "SELECT * FROM tbl_karyawan order by nama_karyawan desc");
                            foreach ($select as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['id_karyawan']; ?></td>
                                    <td><?php echo $data['nama_karyawan']; ?></td>
                                    <td><?php echo $data['jenis_kelamin']; ?></td>
                                    <td><?php echo $data['agama']; ?></td>
                                    <td><?php echo $data['job_desk']; ?></td>
                                    <td><?php echo $data['id_akun']; ?></td>
                                    <td>
                                        <div class="timeline-footer">
                                            <a href="controller/buat_akun.php?id=<?php echo $data['id_karyawan']; ?>" class="btn btn-primary btn-sm">
                                                Buat Akun
                                            </a>
                                            <a href="#" class="btn btn-danger btn-sm edit_karyawan_kan" data-toggle="modal" data-target="#modal-default_edit">Edit</a>
                                            <a href="controller/hapus_karyawan.php?id=<?php echo $data['id_karyawan']; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apa anda yakin?')">Delete</a>
                                        </div>

                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah data karyawan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="controller/simpan_karyawan.php">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>ID Karyawan</label>
                                    <input type="text" class="form-control" name="idkaryawan">
                                </div>
                                <div class="form-group">
                                    <label>Nama Karyawan</label>
                                    <input type="text" class="form-control" name="namakaryawan">
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <select class="form-control select2" style="width: 100%;" name="jeniskelamin">
                                        <option value="0">-- Pilih --</option>
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Agama</label>
                                    <select class="form-control select2" style="width: 100%;" name="agamakarywan">
                                        <option value="0">-- Pilih --</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Kristen Protestan">Kristen Protestan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Job Desk</label>
                                    <input type="text" class="form-control" name="job">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- edit modal -->

<div class="modal fade" id="modal-default_edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah data karyawan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="controller/simpan_edit_karyawan.php">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>ID Karyawan</label>
                                    <input type="text" class="form-control" name="idkaryawan_edit" id="idkaryawan_edit" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Nama Karyawan</label>
                                    <input type="text" class="form-control" name="namakaryawan_edit" id="namakaryawan_edit">
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <select class="form-control select2" style="width: 100%;" name="jeniskelamin_edit" id="jeniskelamin_edit">
                                        <option value="0">-- Pilih --</option>
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Agama</label>
                                    <select class="form-control select2" style="width: 100%;" name="agamakarywan_id" id="agamakarywan_id">
                                        <option value="0">-- Pilih --</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Kristen Protestan">Kristen Protestan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Job Desk</label>
                                    <input type="text" class="form-control" name="job_edit" id="job_edit">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>