<?php
$no = 1;
$select = mysqli_query($connect, "SELECT tbl_even.*, tbl_trainer.* FROM tbl_even
                        LEFT JOIN tbl_trainer on tbl_even.id_trainer=tbl_trainer.id_trainer
                       where id_even =$_GET[id]");
foreach ($select as $data) {
?>
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-12">

                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">Detail Event</h3>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Title Event</label>
                                            <textarea class="form-control" rows="3" readonly><?php echo $data['title']; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <a target="_blank" href="view/print_laporan_peserta.php?id_iven=<?php echo $_GET['id']; ?>" class="btn btn-primary">
                                                PRINT LAPORAN
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tanggal</label>
                                            <input type="text" class="form-control" readonly value="<?php echo date("Y/m/d", strtotime($data['start'])) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Jumlah Peserta</label>
                                            <input type="text" class="form-control" readonly value="<?php echo $data['jumlah_peserta'] ?> Orang">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card card-warning">
                <div class="card-header">
                    <h3 class="card-title">Detail Peserta</h3>
                </div>
            </div>
            <div class="row">
                <div class="card-body">
                    <table id="example2" class="table table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID</th>
                                <th>ID Karyawan</th>
                                <th>Nama_Peserta</th>
                                <th>Jenis Kelamin</th>
                                <!-- <th style="width: 130px;">Sertifikat</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkansaja = mysqli_query($connect, "SELECT * FROM tbl_peserta
                            INNER JOIN tbl_karyawan ON tbl_peserta.id_karyawan=tbl_karyawan.id_karyawan
                            where tbl_peserta.id_even='$_GET[id]' order by tbl_peserta.id_peserta desc");
                            foreach ($tampilkansaja as $rowdatakan) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $rowdatakan['id_peserta']; ?></td>
                                    <td><?php echo $rowdatakan['id_karyawan']; ?></td>
                                    <td><?php echo $rowdatakan['nama_karyawan']; ?></td>
                                    <td><?php echo $rowdatakan['jenis_kelamin']; ?></td>
                                    <!-- <td>
                                        <?php if ($rowdatakan['nilai_sertifikat'] == '0') { ?>
                                            Belum di ada
                                        <?php } else { ?>
                                            <a target="_blank" href="view/print_sertifikat.php?id_even=<?php echo $data['id_even']; ?>">Print</a>
                                        <?php } ?>
                                    </td> -->
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <!-- modal -->
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload Sertifkat</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="controller/action_upload_sertifikat.php" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="hidden" name="id_even" value="<?php echo $_GET['id']; ?>">
                                    <label>Pilih Peserta</label>
                                    <select class="form-control select2" style="width: 100%;" name="id_peserta">
                                        <option value="0">-- Pilih --</option>
                                        <?php
                                        $select_peserta = mysqli_query($connect, "SELECT tbl_peserta.*, tbl_karyawan.nama_karyawan from tbl_peserta
                                        INNER JOIN tbl_karyawan on tbl_peserta.id_karyawan=tbl_karyawan.id_karyawan 
                                        where id_even='$_GET[id]' and sertifikat = '0'");
                                        foreach ($select_peserta as $peserta) {
                                        ?>
                                            <option value="<?php echo $peserta['id_peserta'] ?>"><?php echo $peserta['id_karyawan']; ?> => <?php echo $peserta['nama_karyawan']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Sertifikat</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="exampleInputFile" name="sertifikat">
                                            <label class="custom-file-label" for="exampleInputFile">Pilih File</label>
                                            * Sertifikat berbentuk .pdf
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </div>
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



<?php } ?>