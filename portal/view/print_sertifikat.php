<?php
include '../../app/koneksi.php';
session_start();
// $select = mysqli_query($connect, "SELECT tbl_even.*, tbl_trainer.*, tbl_peserta.id_peserta, tbl_peserta.status_peserta, tbl_peserta.nilai_sertifikat FROM tbl_even
//                         INNER JOIN tbl_trainer on tbl_even.id_trainer=tbl_trainer.id_trainer
//                         INNER JOIN tbl_peserta on tbl_even.id_even=tbl_peserta.id_even
//                         INNER JOIN tbl_karyawan on tbl_peserta.id_karyawan=tbl_karyawan.id_karyawan
//                         where tbl_karyawan.id_karyawan = '$_SESSION[id_akun]' and status_peserta ='Diikuti' and tbl_peserta.id_even='$_GET[id_even]'");

$select = mysqli_query($connect, "SELECT * FROM tbl_karyawan 
 INNER JOIN tbl_peserta ON tbl_karyawan.id_karyawan = tbl_peserta.id_karyawan
 INNER JOIN tbl_even ON tbl_peserta.id_even = tbl_even.id_even 
 where tbl_even.id_even='$_GET[id_even]' and tbl_karyawan.id_karyawan = '$_SESSION[id_akun]'
");
foreach ($select as $data) {
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sertifikat</title>
    </head>

    <body style="border: double;" onload="window.print()">
        <table border="0" align="center" style="width: 100%;">
            <tr>
                <td align="center" style="width: 100px;"><img src="../foto_user/download.png" alt="" style="width: 150px;"></td>
                <td align="center"><b>
                        <h1>PT. TELKOM AKSES</h1>
                    </b></td>
                <td style="width: 150px;"></td>
            </tr>

            <tr>
        
                <td align="center" colspan="3">
                    <br>
                    <u style="font-size: 50px;">SERTIFIKAT</u>
                    <div></div><br>
                    DIBERIKAN KEPADA:
                    <br>
                    <b style="font-size: 50px;"><?php echo $data['nama_karyawan']; ?></b>
                    <br>
                    <br>
                    <div style="font-size: 18px;">Telah menyelesaikan pelatihan <?php echo $data['title']; ?></div>
                    <div style="font-size: 20px;"><b>Dengan Nilai <?php echo $data['nilai_sertifikat']; ?></b></div>
                    <div style="font-size: 18px;">Sertifikat ini diberikan kepada yang bersangkutan untuk dapat digunakan sebagaimana mestinya.</div>
                </td>
        
            </tr>

            <tr>
                <td><br>
                    <br><br><br><br><br><br><br><br><br><br><br>

                </td>
                <td align="right">
                    <table border="0">
                        <tr >
                            <br>
                            <br>
                            <br>
                            <br>

                            <td align="center" style="font-size: 18px;">Padang. <?php echo date('d-m-Y', strtotime($data['end'])); ?></td>
                        </tr>
                        <tr>

                            <td align="center">

                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div style="font-size: 18px;">GM Telkom Akses Sumbagteng</div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>

        </table>
    </body>

    </html>

<?php } ?>