<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <section class="content">

                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <!-- <h3 class="card-title">Selamat Datang</h3> -->

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-12 col-lg-12 order-2 order-md-1">
                                    <div class="row">
                                        <div class="col-12 col-sm-4">
                                            <div class="info-box bg-light">
                                                <div class="info-box-content">
                                                    <span class="info-box-text text-center text-muted">Event Sudah diikuti</span>
                                                    <?php
                                                    $jml_dikuti = mysqli_query($connect, "SELECT * FROM tbl_peserta where id_karyawan = $_SESSION[id_akun] and status_peserta = 'Diikuti'");
                                                    ?>
                                                    <span class="info-box-number text-center text-muted mb-0"><?php echo mysqli_num_rows($jml_dikuti); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4">
                                            <div class="info-box bg-light">
                                                <div class="info-box-content">
                                                    <span class="info-box-text text-center text-muted">Total event Open</span>
                                                    <?php
                                                    $jml_open = mysqli_query($connect, "SELECT * FROM tbl_even where status = 'Open'");
                                                    ?>
                                                    <span class="info-box-number text-center text-muted mb-0"><?php echo mysqli_num_rows($jml_open); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4">
                                            <div class="info-box bg-light">
                                                <div class="info-box-content">
                                                    <span class="info-box-text text-center text-muted">Total Event Proses</span>
                                                    <?php
                                                    $jml_proses = mysqli_query($connect, "SELECT * FROM tbl_even where status = 'Proses'");
                                                    ?>
                                                    <span class="info-box-number text-center text-muted mb-0"><?php echo mysqli_num_rows($jml_proses); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <h4 style="color: red;">
                                                <marquee> SELAMAT DATANG DI TELKOM AKSES. </marquee>
                                            </h4>
                                            <img class="" src="../public/image/index.jpg" alt="Photo" style="width: 100%;height: 500px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </section>
            </div>
        </div>
    </div>
</div>