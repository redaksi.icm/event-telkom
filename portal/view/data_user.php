<div class="card">
    <div class="card-header">
        <h3 class="card-title">DATA USER </h3>
        <br>
        <br>
        <div class="row">
            <div class="col-12">
                <br>
                <br>
                <div class="card">
                    <table id="example2" class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID User</th>
                                <th>Nama User</th>
                                <th>Username</th>
                                <th>Password</th>
                                <th>Level</th>
                                <th>Foto</th>
                                <!-- <th>ID Akun</th> -->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $select = mysqli_query($connect, "SELECT * FROM tbl_user order by id_user desc");
                            foreach ($select as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['id_user']; ?></td>
                                    <td><?php echo $data['nama_user']; ?></td>
                                    <td><?php echo $data['username']; ?></td>
                                    <td><?php echo $data['password']; ?></td>
                                    <td><?php echo $data['level']; ?></td>
                                    <td> <a href="foto_user/<?php echo $data['foto']; ?>" target="_blank">Lihat Foto</a> </td>
                                    <td>
                                        <div class="timeline-footer">
                                            <a href="#" class="btn btn-danger btn-sm edit_user_kan" data-toggle="modal" data-target="#modal-default_edit">Edit</a>
                                            <a href="controller/hapus_user.php?id=<?php echo $data['id_user']; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apa anda yakin?')">Delete</a>

                                        </div>
                                    </td>
                                </tr>

                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal -->

<div class="modal fade" id="modal-default_edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit data user</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="controller/simpan_edi_user.php" enctype="multipart/form-data">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>ID user</label>
                                    <input type="text" class="form-control" name="id_user" id="id_user" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Nama user</label>
                                    <input type="text" class="form-control" name="nama_user" id="nama_user">
                                </div>
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="username" id="username">
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="text" class="form-control" name="passowrd" id="passowrd">
                                </div>
                                <div class="form-group">
                                    <label>Level</label>
                                    <input type="text" class="form-control" readonly name="level" id="level">
                                </div>
                                <div class="form-group">
                                    <label>Foto</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="foto" name="foto">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>