<?php
include "../app/koneksi.php";

if (isset($_GET["act"])) {
    $p = $_GET["act"];
} else {
    $p = 1;
}
switch ($p) {

    case "1":
        require("view/beranda.php");
        break;

    case "2":
        require("view/data_event.php");
        break;

    case "3":
        require("view/tambah_peserta.php");
        break;

    case "4":
        require("view/view_peserta.php");
        break;

    case "5":
        require("view/upload_modul.php");
        break;

    case "6":
        require("view/view_even_peserta.php");
        break;

    case "7":
        require("view/view_even_trainer.php");
        break;

    case "8":
        require('view/event_selesai_peserta.php');
        break;

    case "9":
        require('view/sertifikat.php');
        break;

    case "10":
        require('view/data_karyawan.php');
        break;

    case "11":
        require('view/data_trainer.php');
        break;

    case "12":
        require('view/data_user.php');
        break;

    case "13":
        require('view/input_nilai_sertifikat.php');
        break;
}
