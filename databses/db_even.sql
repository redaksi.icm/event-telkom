-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2020 at 04:38 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_even`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_even`
--

CREATE TABLE `tbl_even` (
  `id_even` int(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `jumlah_peserta` double DEFAULT NULL,
  `lokasi` text DEFAULT NULL,
  `periode` text DEFAULT NULL,
  `id_trainer` int(10) DEFAULT NULL,
  `modul` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_even`
--

INSERT INTO `tbl_even` (`id_even`, `title`, `color`, `start`, `end`, `status`, `jumlah_peserta`, `lokasi`, `periode`, `id_trainer`, `modul`) VALUES
(15, 'Pelatihan Indihome 3P - Jawa Barat', '#FF0000', '2020-07-29 00:00:00', '2020-07-30 00:00:00', 'Closed', 101, 'padang', '1 kali dalam', 5, '0'),
(16, 'FTTH Specification - Jawa Barat', '#0071c5', '2020-07-30 00:00:00', '2020-07-31 00:00:00', 'Proses', 10, 'padang utara', '1 kali dalam se minggu', 5, 'bab 4 sheila.docx');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_karyawan`
--

CREATE TABLE `tbl_karyawan` (
  `id_karyawan` varchar(255) NOT NULL,
  `nama_karyawan` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `job_desk` varchar(255) DEFAULT NULL,
  `id_akun` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_karyawan`
--

INSERT INTO `tbl_karyawan` (`id_karyawan`, `nama_karyawan`, `jenis_kelamin`, `agama`, `job_desk`, `id_akun`) VALUES
('10003', 'Sheila', 'Perempuan', 'Islam', 'sdjosidkjfdsf', '10003'),
('10004', 'Sheila 2', 'Perempuan', 'Islam', 'sdjosidkjfdsf', '10004');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_peserta`
--

CREATE TABLE `tbl_peserta` (
  `id_peserta` int(20) NOT NULL,
  `id_karyawan` varchar(255) DEFAULT NULL,
  `id_even` int(255) DEFAULT NULL,
  `status_peserta` varchar(255) DEFAULT NULL,
  `sertifikat` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_peserta`
--

INSERT INTO `tbl_peserta` (`id_peserta`, `id_karyawan`, `id_even`, `status_peserta`, `sertifikat`) VALUES
(13, '10003', 15, 'Diikuti', 'BAB 1 3 sheila ACC.pdf'),
(14, '10003', 16, '0', '0'),
(15, '10004', 16, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_trainer`
--

CREATE TABLE `tbl_trainer` (
  `id_trainer` int(20) NOT NULL,
  `nama_trainer` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT NULL,
  `id_akun` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_trainer`
--

INSERT INTO `tbl_trainer` (`id_trainer`, `nama_trainer`, `jenis_kelamin`, `id_akun`) VALUES
(5, 'Saparudin. S.kom', 'Laki-laki', '1001');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(255) NOT NULL,
  `nama_user` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `foto` text DEFAULT NULL,
  `id_akun` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `nama_user`, `username`, `password`, `level`, `foto`, `id_akun`) VALUES
(1, 'Administrator', 'admin', '12345', 'Admin', '-', '-'),
(16, 'Saparudin. S.kom', '1001', '12345', 'Trainer', 'laravel-logo.png', '1001'),
(17, 'Sheila', '10003', '12345', 'Peserta', '-', '10003'),
(18, 'Sheila 2', '10004', '12345', 'Peserta', 'I_have_an_dea_desktop_background_by_ZerCareer_2048x2048.webp', '10004');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_even`
--
ALTER TABLE `tbl_even`
  ADD PRIMARY KEY (`id_even`) USING BTREE;

--
-- Indexes for table `tbl_karyawan`
--
ALTER TABLE `tbl_karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `tbl_peserta`
--
ALTER TABLE `tbl_peserta`
  ADD PRIMARY KEY (`id_peserta`);

--
-- Indexes for table `tbl_trainer`
--
ALTER TABLE `tbl_trainer`
  ADD PRIMARY KEY (`id_trainer`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_even`
--
ALTER TABLE `tbl_even`
  MODIFY `id_even` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_peserta`
--
ALTER TABLE `tbl_peserta`
  MODIFY `id_peserta` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_trainer`
--
ALTER TABLE `tbl_trainer`
  MODIFY `id_trainer` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
